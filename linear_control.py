import numpy as np
# import pandas as pd
import os
import traceback
import time
import re
import pickle
import sys
import cv2
import cv2.aruco as aruco
from numpy import linalg as LA
import json
import matplotlib.pyplot as plt
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
np.set_printoptions(suppress=True)
import URDriver

ERROR_TOTAL = []

def get_cnt(image):

    ret_el = True

    img = image.copy()

    image_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    n = int(max(np.mean(image.shape[:2])/200, 0))

    kernel = np.ones((3, 3))
    mode = cv2.MORPH_ERODE

    image_hsv[:, :, 0] += 40

    red_mask = cv2.inRange(image_hsv, (0, 70, 50), (45, 255, 255))

    white_mask = cv2.inRange(image_hsv, (0, 0, 100), (255, 30, 255))
    white_mask = cv2.morphologyEx(white_mask, mode, kernel=kernel, iterations=n)

    blue_mask = cv2.inRange(image_hsv, (140, 50, 0), (170, 255, 255))
    blue_mask = cv2.morphologyEx(blue_mask, mode, kernel=kernel, iterations=n)

    green_mask = cv2.inRange(image_hsv, (80, 50, 0), (110, 255, 255))
    green_mask = cv2.morphologyEx(green_mask, mode, kernel=kernel, iterations=n)

    black_mask = cv2.inRange(image_hsv, (0, 0, 0), (255, 30, 60))
    black_mask = cv2.morphologyEx(black_mask, mode, kernel=kernel, iterations=n)

    black_mask += green_mask + blue_mask

    markers = np.int32(red_mask / 255 + white_mask / 255 * 2 + black_mask / 255 * 3)

    markers3 = cv2.watershed(img, markers)
    th = np.uint8(markers3/3)
    th = cv2.morphologyEx(th, cv2.MORPH_OPEN, kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)), iterations=8)

    contours0, hierarchy = cv2.findContours(th.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    max_white = 0
    be_ellipse = ((0, 0), (0, 0), 0)
    be_cnt = np.array([])

    for cnt in contours0:
        if len(cnt) > 50:
            ellipse = cv2.fitEllipse(cnt)
            y, x, w, h = cv2.boundingRect(cnt)
            img_crop = np.array(th[int(x):int(x+h), int(y):int(y+w)], dtype="bool")
            mask = np.zeros((h, w))
            (cx, cy), (ew, eh), a = ellipse
            cv2.ellipse(mask, ((cx-y, cy-x), (ew, eh), a), (1, 1, 1), -1)
            mask = np.array(mask, dtype="bool")
            img_crop = np.array(img_crop*mask, dtype="uint8")
            if max_white < img_crop.sum()/(eh*ew*np.pi/4):
                max_white = img_crop.sum()/(eh*ew*np.pi/4)
                be_ellipse = ellipse
                be_cnt = cnt
    be_cnt = be_cnt.reshape(-1, 2)
    if be_cnt.sum() == 0:
        ret_el = False

    return ret_el, be_ellipse, be_cnt, th

def get_oreint(img, be_ellipse, be_cnt):

    ret = 1
    if be_cnt.sum() == 0:
        return 0, (0, 0), (0, 0)
    left = np.min(list(map(lambda i: i[0], be_cnt)))
    right = np.max(list(map(lambda i: i[0], be_cnt)))
    top = np.min(list(map(lambda i: i[1], be_cnt)))
    bottom = np.max(list(map(lambda i: i[1], be_cnt)))

    d = 50
    sh = (int(max(top - d, 0)), int(max(left - d, 0)), int(bottom + d), int(right + d))

    port = img[sh[0]:sh[2], sh[1]:sh[3], :]

    w, h = port.shape[:2]

    n = max((int(np.round(np.mean([w, h]))/50)-1), 1)

    print("      ", n)

    mask = np.zeros(port.shape[:2])

    ellipse = ((be_ellipse[0][0]-left+d, be_ellipse[0][1]-top+d),
               (be_ellipse[1][0]+2*d, be_ellipse[1][1]+2*d),
               be_ellipse[2])

    cv2.ellipse(mask, ellipse, (1, 1, 1), -1)

    imga = port.copy()
    imga[:, :, 0] = imga[:, :, 0] * mask
    imga[:, :, 1] = imga[:, :, 1] * mask
    imga[:, :, 2] = imga[:, :, 2] * mask

    image_hsv = cv2.cvtColor(imga, cv2.COLOR_BGR2HSV)
    kernel = np.ones((3, 3))

    blue_mask = cv2.inRange(image_hsv, (90, 50, 30), (150, 255, 255))
    blue_mask = cv2.morphologyEx(blue_mask, cv2.MORPH_OPEN, kernel=kernel, iterations=n)

    green_mask = cv2.inRange(image_hsv, (30, 50, 30), (70, 255, 255))
    green_mask = cv2.morphologyEx(green_mask, cv2.MORPH_OPEN, kernel=kernel, iterations=n)

    port[:, :, 2] = np.uint8(np.clip(np.uint16(port[:, :, 2])+np.uint16(blue_mask)+np.uint16(green_mask), 0, 255))


    a = blue_mask.copy()
    if a.sum() == 0:
        ret = 0
        blue_mark = (0, 0)
    else:
        blue_mark = (a * np.mgrid[0:a.shape[0], 0:a.shape[1]]).sum(1).sum(1) / a.sum()

    a = green_mask.copy()
    if a.sum() == 0:
        ret = 0
        green_mark = (0, 0)
    else:
        green_mark = (a * np.mgrid[0:a.shape[0], 0:a.shape[1]]).sum(1).sum(1) / a.sum()


    top_d = 0 if top - d < 0 else top-d
    left_d = 0 if left - d < 0 else left-d

    return ret, (int(blue_mark[1]+left_d), int(blue_mark[0]+top_d)), \
                (int(green_mark[1]+left_d), int(green_mark[0]+top_d))

def findArucoMarkers(img, makrkersize=6, totalMarkers=250, draw=True):
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    arucoDict = aruco.Dictionary_get(aruco.DICT_4X4_250)
    arucoParam = aruco.DetectorParameters_create()
    bboxs, ids, rej = aruco.detectMarkers(imgGray, 
                                          arucoDict, parameters=arucoParam)
    
    if (ids is not None and len(ids) > 0):
        # return [(int((bboxs[0][0][0][0] + bboxs[0][0][2][0])/2), int((bboxs[0][0][0][1]+ bboxs[0][0][2][1]) / 2)), bboxs[0][0][0], bboxs[0][0][1], bboxs[0][0][2], bboxs[0][0][3]]
        # return [(int((bboxs[0][0][0][0] + bboxs[0][0][2][0])/2), int((bboxs[0][0][0][1]+ bboxs[0][0][2][1]) / 2))]
        return [bboxs[0][0][0], bboxs[0][0][1], bboxs[0][0][2], bboxs[0][0][3]]
    
def findPortMarkers(img):
    img = cv2.GaussianBlur(img, (5, 5), 10)
    flag, best_ellipse, best_cnt, th_o = get_cnt(img)

    if not flag:
        best_ellipse = ((0, 0), (20, 20), 1)

    h, w = th_o.shape
    
    print("FLAG", flag)
    if flag:
        flag2, blue, green = get_oreint(img, best_ellipse, best_cnt)
        angle = np.radians(best_ellipse[2])
        center = best_ellipse[0]
        im_blue = (2*center[0] - blue[0], 2*center[1] - blue[1])
        im_green = (2*center[0] - green[0], 2*center[1] - green[1])
        return [center, im_blue, im_green, blue, green]
    else:
        return []

class Task:
    def __init__(self, camera_config, features_desired_positions):
        self.features_desired_positions = features_desired_positions
        self.features_current_positions = []
        self.cap = cv2.VideoCapture(camera_config["port"])
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, camera_config["resolution"][0])
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, camera_config["resolution"][1])
        self.principal_point = camera_config["principal_point"]
        self.focal_length = camera_config["focal_length"]
        self.flag = False
        self.flag2 = False
        self.detecting_function = findArucoMarkers
        self.max_area = 0
        self.k = 1.0

    def calculate_J(self, u, v, Z):
        J = np.array(
            [
                [-1/Z, 0, u/Z],
                [0, -1/Z, v/Z]
            ]
        )  
        return J
    
    def get_current_features(self):
        scs, img = self.cap.read()
        if scs:
            self.features_current_positions = self.detecting_function(img)
        else:
            self.features_current_positions = []
       
        
    def calculate_error(self):
        global ERROR_TOTAL
        error = np.zeros(2*len(self.features_desired_positions))
        if (len(self.features_desired_positions) == len(self.features_current_positions)):
            cnt = 0
            for i in range(0, len(self.features_desired_positions)):
                f = self.features_current_positions[i]
                u = (f[0] - self.principal_point[0]) / self.focal_length[0]
                v = (f[1] - self.principal_point[1]) / self.focal_length[1] 
                f2 = self.features_desired_positions[i]
                u2 = (f2[0] - self.principal_point[0]) / self.focal_length[0]
                v2 = (f2[1] - self.principal_point[1]) / self.focal_length[1] 
                error[cnt] = u - u2
                error[cnt + 1] = v - v2
                cnt += 2
        ERROR_TOTAL.append(np.linalg.norm(error))
        return error
    
    def show(self):
        delta = 10
        scs, image = self.cap.read()
        image = cv2.GaussianBlur(image, (5, 5), 10)
        for pos in self.features_desired_positions:
            pos = [int(x) for x in pos]
            cv2.line(image, (pos[0] - delta, pos[1]), (pos[0] + delta, pos[1]), (255, 0, 0))
            cv2.line(image, (pos[0], pos[1] - delta), (pos[0], pos[1] + delta), (255, 0, 0))
        if self.features_current_positions is not None and len(self.features_current_positions) > 0:
            for pos in self.features_current_positions:
                pos = [int(x) for x in pos]
                cv2.line(image, (pos[0] - delta, pos[1]), (pos[0] + delta, pos[1]), (0, 0, 255))
                cv2.line(image, (pos[0], pos[1] - delta), (pos[0], pos[1] + delta), (0, 0, 255))
            for i in range(len(self.features_current_positions)):
                pos1 = self.features_current_positions[i]
                pos2 = features_desired_positions[i]
                pos1 = [int(x) for x in pos1]
                pos2 = [int(x) for x in pos2]
                cv2.line(image, tuple(pos1), tuple(pos2), (0, 255, 0))

        #resize image
        scale_percent = 60
        new_width = int(image.shape[1]*scale_percent/100)
        new_height = int(image.shape[0]*scale_percent/100)
        new_dim = (new_width,new_height)

        resized = cv2.resize(image, new_dim, interpolation=cv2.INTER_AREA)
        cv2.imshow("Image", resized)

    def calculate_velocity(self):
        self.get_current_features() 
        # contour = np.array([[self.features_current_positions[1]], [self.features_current_positions[2]], [self.features_current_positions[3]], [self.features_current_positions[4]]])
        # area = cv2.contourArea(contour) 
        # print("AREA:", area)

        if self.features_current_positions is None or len(self.features_current_positions) <= 0:
            return np.zeros(6)
        
        Lambda = 2.33 / 1000
        Z = 0.2
        J_total = []
        f_ = []
        for f in self.features_current_positions:
            u = (f[0] - self.principal_point[0]) / self.focal_length[0]
            v = (f[1] - self.principal_point[1]) / self.focal_length[1] 
            f_.append(u)
            f_.append(v)
            if len(J_total) <= 0:
                J_total = self.calculate_J( u, v, Z)
            else:
                J = self.calculate_J( u, v, Z)
                J_total = np.concatenate((J_total, J), axis=0)
            if f[0] <= 80 or f[0] >= 540 or f[1] <= 80 or f[1] >= 400:
                self.flag = True
        print("F:", f_)
        print("Condition number",np.linalg.cond(J_total))
        # J_inv = np.linalg.pinv(J_total)
        print(J_total)
        J_inv = np.linalg.inv(np.transpose(J_total) @ J_total) @ np.transpose(J_total)
        k = 0.1
        E = np.array(self.calculate_error())
 
        E = k*np.transpose(E)
        print("ERROR:", E)
    
        vel = J_inv @ E
        print("PRODUCT:", J_inv @ J_total)
        vel = np.concatenate((vel, np.array([0, 0, 0])), axis=0)
        return -vel

if __name__ == '__main__':
    with open("./IBVS/config/camera.json") as file:
        camera_config = json.load(file)
    

    # Setup robot
    ip = '192.168.88.5'
    robot1 = URDriver.UniversalRobot(ip)

    HOME_POSITION = np.array([-90, -90, -90, 0, 90, 0]) * np.pi / 180
    NUM_JOINTS = 6
    # Joint movement
    robot1.control.moveJ(HOME_POSITION)

    # Cartesian space movements
    up_height = 0.01

    orient = np.array([0.0, 3.14, 0.0])
    # orient = np.array([2.119, 2.318, 0.002])	
    position = np.array([0.0346, -0.6279, 0.6])
    pose = np.concatenate((position, orient))
   
    # Movement configuration
    speed = 0.5
    acceleration = 0.5
    dt = 1.0/500
    robot1.control.moveL(pose, speed, acceleration)
    delta = 100
    features_desired_positions = camera_config["features"]
   
    new_task = Task(camera_config, features_desired_positions)

    robot1.control.servoStop()
    robot_model = URDriver.robot.RobotModel('urdf_model/ur5e_right.urdf','world', 'tool0')
    vel = new_task.calculate_velocity()
    # robot1.control.teachMode()
    while True:
        
        start = time.time()
        robot1.update_state()
        # print("POSITION:", robot1.receive.getActualTCPPose())
        new_task.show()

        vel = new_task.calculate_velocity()
       
        robot1.update_state()
        q = robot1.state.q   
        R = robot_model.rot(q)
        # print("POS: ", robot_model.pose_angvec(q))
        # Rotate_matrix = np.kron(np.eye(2,2), R)
        
        vel_in_base = R @ vel
       
        print("Tool:", vel)
        print("Base:", vel_in_base)
        
        
        robot1.control.speedL(vel_in_base, acceleration, dt)
        
        end = time.time()
        duration = end - start
        if cv2.waitKey(1) == ord('q'):
            break
        if duration < dt:
            time.sleep(dt - duration)
        
